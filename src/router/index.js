import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import projectRoutes from './modules/project'
import systemRoutes from './modules/system'
import studyRoutes  from './modules/study'

export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

export const allRoutes = []

function addAll(array) {
  array.forEach((v) => allRoutes.push(v))
}

function test() {
  allRoutes.splice(0, allRoutes.length)
  addAll(constantRoutes)
  addAll(projectRoutes)
  addAll(systemRoutes)
  addAll(studyRoutes)
  return allRoutes
}

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: test()
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
