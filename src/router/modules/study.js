import StudyLayout from '@/layout/study'

const studyRoutes = [
  {
    path: '/study',
    component: StudyLayout,
    redirect: '/study/event',
    children: [
      {
        path: '/study/event',
        name: 'Study',
        component: () => import('@/views/pages/study/event'),
        meta: { title: '事件管理', icon: 'event' }
      }
    ]
  },
  {
    path: '/study/image',
    component: StudyLayout,
    children: [
      {
        path: 'index',
        name: 'StudyImage',
        component: () => import('@/views/pages/study/image'),
        meta: { title: '图片轮播', icon: 'imagechange' }
      }
    ]
  },
  {
    path: '/study/worktype',
    component: StudyLayout,
    children: [
      {
        path: 'index',
        name: 'WorkType',
        component: () => import('@/views/pages/study/worktype'),
        meta: { title: '工种管理', icon: 'role' }
      }
    ]
  },
  {
    path: '/study/data',
    component: StudyLayout,
    children: [
      {
        path: 'index',
        name: 'Data',
        component: () => import('@/views/pages/study/data'),
        meta: { title: '工种资料', icon: 'studydata' }
      }
    ]
  }

]

export default studyRoutes
