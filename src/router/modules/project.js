import ProjectLayout from '@/layout/project'

const projectRoutes = [
  {
    path: '/',
    component: ProjectLayout,
    meta: {
      title: '项目设置',
      icon: 'setproject'
    },
    redirect: '/project/buildings',
    children: [
      {
        path: '/project/buildings',
        name: 'Buildings',
        component: () => import('@/views/pages/project/manage/building/index'),
        meta: { title: '楼栋管理' }
      },
      {
        path: '/project/teams',
        name: 'Teams',
        component: () => import('@/views/pages/project/manage/teams/index'),
        meta: { title: '班组管理' }
      },
      {
        path: '/project/tenders',
        name: 'Tenders',
        component: () => import('@/views/pages/project/manage/tenders/index'),
        meta: { title: '标段管理' }
      },
      {
        path: '/project/checks',
        name: 'CheckType',
        component: () => import('@/views/pages/project/manage/checks/index'),
        meta: { title: '检查项管理' }
      },
      {
        path: '/project/nature',
        name: 'Nature',
        component: () => import('@/views/pages/project/manage/nature/index'),
        meta: { title: '检查性质管理' }
      },
      {
        path: '/project/drawers',
        name: 'Drawers',
        component: () => import('@/views/pages/project/manage/drawers/index'),
        meta: { title: '图纸管理' }
      }
    ]
  },

  {
    path: '/project/measure',
    component: ProjectLayout,
    redirect: '/project/measure/points',
    meta: { title: '实测实量', icon: 'measure', menuId: 22  },
    children: [
      {
        path: '/project/measure/points',
        name: 'Points',
        component: () => import('@/views/pages/project/measure/point/index'),
        meta: { title: '测点管理' }
      },
      {
        path: '/project/measure/drawers',
        name: 'Drawers',
        component: () => import('@/views/pages/project/measure/draw/index'),
        meta: { title: '图纸管理' }
      },
      {
        path: '/project/measure/problem',
        name: 'Problem',
        component: () => import('@/views/pages/project/measure/critical/index'),
        meta: { title: '爆点管理' }
      }
    ]
  },

  {
    path: '/project/drawerPage',
    name: 'DrawerPage',
    hidden: true,
    component: () => import('@/views/pages/project/measure/components/DrawPage'),
    meta: { title: '测点管理', icon: 'tree' }
  },

  {
    path: '/project/material',
    component: ProjectLayout,
    redirect: '/project/material/statistics',
    meta: {
      title: '物资管理',
      icon: 'material', menuId: 9
    },
    children: [
      {
        path: '/project/material/statistics',
        name: 'Statistics',
        component: () => import('@/views/pages/project/supplies/index'),
        meta: { title: '物资统计' }
      }
    ]
  },

  {
    path: '/project/construct',
    component: ProjectLayout,
    redirect: '/project/construct/points',
    meta: { title: '施工日志', icon: 'construct', menuId: 4  },
    children: [
      {
        path: '/project/construct/log',
        name: 'Log',
        component: () => import('@/views/pages/project/construct/log/index'),
        meta: { title: '施工日志' }
      },
      {
        path: '/project/construct/content',
        name: 'Content',
        component: () => import('@/views/pages/project/construct/content/index'),
        meta: { title: '施工内容' }
      },
      {
        path: '/project/construct/contentType',
        name: 'ContentType',
        component: () => import('@/views/pages/project/construct/type/index'),
        meta: { title: '内容类型' }
      },
      {
        path: '/project/construct/permission',
        name: 'Permission',
        component: () => import('@/views/pages/project/construct/permission/index'),
        meta: { title: '权限管理' }
      },
      {
        path: '/project/construct/problem',
        name: 'Problem',
        component: () => import('@/views/pages/project/construct/problem/index'),
        meta: { title: '存在问题' }
      },
      {
        path: '/project/construct/machine',
        name: 'Machine',
        component: () => import('@/views/pages/project/construct/machine/index'),
        meta: { title: '机械问题' }
      },
      {
        path: '/project/construct/subject',
        name: 'Subject',
        component: () => import('@/views/pages/project/construct/subject/index'),
        meta: { title: '主材量管理' }
      }
    ]
  },

  {
    path: '/project/quality',
    component: ProjectLayout,
    redirect: '/project/quality/inspection',
    meta: { title: '质量管理', icon: 'quality', menuId: 6  },
    children: [
      {
        path: '/project/quality/inspection',
        name: 'Inspection',
        component: () => import('@/views/pages/project/quality/inspection/index'),
        meta: { title: '质量检查单' }
      },
      {
        path: '/project/quality/rectification',
        name: 'Rectification',
        component: () => import('@/views/pages/project/quality/rectification/index'),
        meta: { title: '质量整改单' }
      },
      {
        path: '/project/quality/reward',
        name: 'Reward',
        component: () => import('@/views/pages/project/quality/reward/index'),
        meta: { title: '质量奖惩单' }
      }
    ]
  },

  {
    path: '/project/safety',
    component: ProjectLayout,
    redirect: '/project/safety/inspection',
    meta: { title: '安全管理', icon: 'safe', menuId: 5 },
    children: [
      {
        path: '/project/safety/inspection',
        name: 'Inspection',
        component: () => import('@/views/pages/project/safety/inspection/index'),
        meta: { title: '安全检查单'}
      },
      {
        path: '/project/safety/rectification',
        name: 'Rectification',
        component: () => import('@/views/pages/project/safety/rectification/index'),
        meta: { title: '安全整改单'}
      },
      {
        path: '/project/safety/reward',
        name: 'Reward',
        component: () => import('@/views/pages/project/safety/reward/index'),
        meta: { title: '安全奖惩单'}
      }
    ]
  },

  {
    path: '/project/constructionTask',
    component: ProjectLayout,
    redirect: '/project/constructionTask/list',
    meta: { title: '施工任务单管理', icon: 'constructtask', menuId: 24 },
    children: [
      {
        path: '/project/constructionTask/list',
        name: 'Inspection',
        component: () => import('@/views/pages/project/tasklist'),
        meta: { title: '施工任务单' }
      }
    ]
  }

  // {
  //   path: '/project/attendance',
  //   component: ProjectLayout,
  //   redirect: '/project/attendance/location',
  //   meta: { title: '考勤管理', icon: 'example' },
  //   children: [
  //     {
  //       path: '/project/attendance/location',
  //       name: 'Location',
  //       component: () => import('@/views/dashboard/index'),
  //       meta: { title: '考勤地点', icon: 'table' }
  //     },
  //     // {
  //     //   path: '/project/attendance/model',
  //     //   name: 'Model',
  //     //   component: () => import('@/views/tree/index'),
  //     //   meta: { title: '考勤模版', icon: 'tree' }
  //     // },
  //     // {
  //     //   path: '/project/attendance/record',
  //     //   name: 'Record',
  //     //   component: () => import('@/views/tree/index'),
  //     //   meta: { title: '考勤记录', icon: 'tree' }
  //     // }
  //   ]
  // },

]

export default projectRoutes
