import SystemLayout from '@/layout/system'

const systemRoutes = [
  {
    path: '/system',
    component: SystemLayout,
    redirect: '/system/user',
    children: [
      {
        path: '/system/user',
        name: 'User',
        component: () => import('@/views/pages/system/user'),
        meta: { title: '用户管理', icon: 'user' }
      }
    ]
  },
  {
    path: '/system/project',
    component: SystemLayout,
    children: [
      {
        path: 'index',
        name: 'Project',
        component: () => import('@/views/pages/system/project'),
        meta: { title: '项目列表', icon: 'project' }
      }
    ]
  },
  {
    path: '/system/department',
    component: SystemLayout,
    children: [
      {
        path: 'index',
        name: 'User',
        component: () => import('@/views/pages/system/department'),
        meta: { title: '部门管理', icon: 'department' }
      }
    ]
  },

  {
    path: '/system/role',
    component: SystemLayout,
    children: [
      {
        path: 'index',
        name: 'User',
        component: () => import('@/views/pages/system/role'),
        meta: { title: '角色管理', icon: 'role' }
      }
    ]
  },

  {
    path: '/system/menu',
    component: SystemLayout,
    children: [
      {
        path: 'index',
        name: 'User',
        component: () => import('@/views/pages/system/menu'),
        meta: { title: '菜单管理', icon: 'menu' }
      }
    ]
  },

  {
    path: '/system/worktype',
    component: SystemLayout,
    children: [
      {
        path: 'index',
        name: 'User',
        component: () => import('@/views/pages/system/worktype'),
        meta: { title: '工种管理', icon: 'worktype' }
      }
    ]
  },

  {
    path: '/system/newsinfo',
    component: SystemLayout,
    children: [
      {
        path: 'index',
        name: 'User',
        component: () => import('@/views/pages/system/newsinfo'),
        meta: { title: '新闻资讯', icon: 'newsinfo' }
      }
    ]
  },
  {
    path: '/system/guize',
    component: SystemLayout,
    redirect: '/system/guize',
    meta: { title: '考勤管理', icon: 'kaoqin'},
    children: [
      {
        path: '/system/guize',
        name: 'guize',
        component: () => import('@/views/pages/system/guize'),
        meta: { title: '规则设置' }
      },
      {
        path: '/system/kaoqin',
        name: 'kaoqin',
        component: () => import('@/views/pages/system/kaoqin'),
        meta: { title: '模板管理' }
      }
    ]
  },
 
  {
    path: '/system/notice',
    component: SystemLayout,
    children: [
      {
        path: 'index',
        name: 'Notice',
        component: () => import('@/views/pages/system/notice'),
        meta: { title: '公告管理', icon: 'notice' }
      }
    ]
  },
  {
    path: '/system/imagechange',
    component: SystemLayout,
    children: [
      {
        path: 'index',
        name: 'ImageChange',
        component: () => import('@/views/pages/system/imagechange'),
        meta: { title: '轮播图片', icon: 'imagechange' }
      }
    ]
  },
  {
    path: '/system/feedback',
    component: SystemLayout,
    children: [
      {
        path: 'index',
        name: 'FeedBack',
        component: () => import('@/views/pages/system/feedback'),
        meta: { title: '意见反馈', icon: 'feedback' }
      }
    ]
  }

]

export default systemRoutes
