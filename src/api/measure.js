import request from '@/utils/request'

export function addMeasurePaper(params) {
  return request({
    url: '/MeasurePaper/add',
    method: 'post',
    data: params
  })
}

export function deleteMeasurePaper(params) {
  return request({
    url: '/MeasurePaper/delete',
    method: 'post',
    data: params
  })
}

export function getMeasurePaperList(params) {
  return request({
    url: '/MeasurePaper/select',
    method: 'post',
    data: params
  })
}

export function getMeasureSiteBuildingList(params) {
  return request({
    url: '/MeasureSite/selectList',
    method: 'post',
    data: params
  })
}

export function getMeasureSiteListByPid(params) {
  return request({
    url: '/MeasureSite/selectListByPids',
    method: 'post',
    data: params
  })
}

export function addMeasureBuilding(params) {
  return request({
    url: '/MeasureSite/add',
    method: 'post',
    data: params
  })
}

export function getMeasureBuildingList(params) {
  return request({
    url: '/MeasureSite/selectList',
    method: 'post',
    data: params
  })
}

export function getMeasurePaperLists(params) {
  return request({
    url: '/MeasurePaper/selectList',
    method: 'post',
    data: params
  })
}

export function deleteMeasureSite(params) {
  return request({
    url: '/MeasureSite/delete',
    method: 'post',
    data: params
  })
}

export function getProjectCheckTypeLists(params) {
  return request({
    url: '/ProjectCheckType/selectList',
    method: 'post',
    data: params
  })
}

export function addMeasurePoint(params) {
  return request({
    url: '/MeasurePaper/pushPoint',
    method: 'post',
    data: params
  })
}

export function getListByProjectId(params) {
  return request({
    url: '/JasoUser/getListByProjectId',
    method: 'post',
    data: params
  })
}

export function updateMeasureProblem(params) {
  return request({
    url: '/MeasureProblem/add',
    method: 'post',
    data: params
  })
}
export function deleteMeasureProblem(params){
  return request({
    url: '/MeasureProblem/delete',
    method: 'post',
    data: params
  })
}
export function rectifyMeasureProblem(params) {
  return request({
    url: '/MeasureProblem/setProcess',
    method: 'post',
    data: params
  })
}

export function checkMeasureProblem(params) {
  return request({
    url: '/MeasureProblem/check',
    method: 'post',
    data: params
  })
}

export function getMeasureProblemList(params) {
  return request({
    url: '/MeasureProblem/select',
    method: 'post',
    data: params
  })
}
//重新加载楼栋信息
export function initInfoList(params){
  return request({
    url: '/MeasureSite/tongbu',
    method: 'post',
    data: params
  })
}
//获取楼栋信息
export function getMeasureBuildingInfoList(params){
  return request({
    url: '/MeasureSite/getBuildingInfo',
    method: 'post',
    data: params
  })
}
//获取楼栋下面的房间数据
export function getRoomInfoList(params){
  return request({
    url: '/MeasureSite/selectListByPids',
    method: 'post',
    data: params
  })
}
//获取同步的所有户型
export function getAllApartment(params){
  return request({
    url: '/MeasureSite/getAllApartment',
    method: 'post',
    data: params
  })
}
//获取当前图纸下的所有测点信息(不包括录入的测点数据)
export function getPaperPointList(params){
  return request({
    url: '/MeasurePaper/getPointList',
    method: 'post',
    data: params
  })
}
//获取该项目实测实量的所有户型图纸
export function getMeasurePaperInfoList(params){
  return request({
    url: '/MeasurePaper/selectList',
    method: 'post',
    data: params
  })
}
//更新房间的具体信息
export function updateMeasureSiteInfoByMeasureSiteId(params){
  return request({
    url: '/MeasureSite/update',
    method: 'post',
    data: params
  })
}
//获取检查人列表
export function getAboutUserList(params) {
  return request({
    url: '/JasoUser/getUserListByProjectId',
    method: 'post',
    data: params
  })
}
