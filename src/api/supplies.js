import request from '@/utils/request'

export function getMaterialList(params) {
  return request({
    url: '/Material/select',
    method: 'post',
    data: params
  })
}

export function selectMaterialList(params) {
  return request({
    url: '/Material/selectList',
    method: 'post',
    data: params
  })
}

export function importMaterialList(params) {
  return request({
    url: '/Material/web/importMaterial',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
  })
}

export function addMaterial(params) {
  return request({
    url: '/Material/add',
    method: 'post',
    data: params
  })
}

export function deleteMaterial(params) {
  return request({
    url: '/Material/delete',
    method: 'post',
    data: params
  })
}

export function getMaterialTypeList(params) {
  return request({
    url: '/MaterialType/select',
    method: 'post',
    data: params
  })
}

export function addMaterialType(params) {
  return request({
    url: '/MaterialType/add',
    method: 'post',
    data: params
  })
}

export function deleteMaterialType(params) {
  return request({
    url: '/MaterialType/delete',
    method: 'post',
    data: params
  })
}

export function getMaterialLogList(params) {
  return request({
    url: '/MaterialLog/select',
    method: 'post',
    data: params
  })
}

export function deleteMaterialLog(params) {
  return request({
    url: '/MaterialLog/delete',
    method: 'post',
    data: params
  })
}

export function addMaterialLog(params) {
  return request({
    url: '/MaterialLog/addPC',
    method: 'post',
    data: params
  })
}

export function selectMaterialTypeList(params) {
  return request({
    url: '/MaterialType/selectPage',
    method: 'post',
    data: params
  })
}
