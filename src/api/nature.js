import request from '@/utils/request'

export function addNature(params) {
  return request({
    url: '/Nature/add',
    method: 'post',
    data: params
  })
}

export function deleteNatureList(params) {
  return request({
    url: '/Nature/delete',
    method: 'post',
    data: params
  })
}

export function getNatureList(params) {
  return request({
    url: '/Nature/select',
    method: 'post',
    data: params
  })
}
