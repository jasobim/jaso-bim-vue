import request from '@/utils/request'
export function formatDate(date) {
    let Y = date.getFullYear() + '-';
    let M = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) + '-' : date.getMonth() + 1 + '-';
    let D = date.getDate() < 10 ? '0' + date.getDate() + ' ' : date.getDate() + ' ';
    let h = date.getHours() < 10 ? '0' + date.getHours() + ':' : date.getHours() + ':';
    let m = date.getMinutes()  < 10 ? '0' + date.getMinutes() + ':' : date.getMinutes() + ':';
    let s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
    return Y + M + D + h + m + s;
}
export function addFeedback(params){
    return request({
        url: '/Feedback/add',
        method: 'post',
        data:params
    })
}
//部门管理控制器
    //公用tree方法
export function selectDepartmentTree(params){
    return request({
        url: '/Department/selectTree',
        method: 'post',
        data:params
    })
}
export function selectMenuTree(params){
    return request({
        url: '/Menu/selectTree',
        method: 'post',
        data:params
    })
}
/*列表获取*/
export function selectDepartmentByPid(params){
    return request({
        url: '/Department/selectByPid',
        method: 'post',
        data:params
    })
}
export function addDepartment(params){
    return request({
        url: '/Department/selectByPid',
        method: 'post',
        data:params
    })
}
export function addDepartments(params){
    return request({
        url: '/Department/adds',
        method: 'post',
        data:params
    })
}
export function deleteDepartment(params){
    return request({
        url: '/Department/delete',
        method: 'post',
        data:params
    })
}
export function updateDepartment(params){
    return request({
        url: '/Department/update',
        method: 'post',
        data:params
    })
}
export function selectProjectLists(params){
    return request({
        url: '/Project/selectList',
        method: 'post',
        data:params
    })
}
export function getUserPcDepartmentTreeList(params){
    return request({
        url: '/JasoUser/selectDepartmentTree',
        method: 'post',
        data:params
    })
}
 /*组织架构列表加载*/
 export function getDepartmentList(params){
    return request({
        url: '/Department/select',
        method: 'post',
        data:params
    })
}