import request from '@/utils/request'
//试题管理
//学习题目列表获取
export function getStudyDataList(params){
    return request({
        url: '/StudyData/select',
        method: 'post',
        data:params
    })
}
//学习题目新增
export function addStudyData(params){
    return request({
        url: '/StudyData/add',
        method: 'post',
        data:params
    })
}
//学习题目删除
export function deleteStudyData(params){
    return request({
        url: '/StudyData/delete',
        method: 'post',
        data:params
    })
}
//试题导入
export function importStudyData(params){
    return request({
        url: '/StudyData/import',
        method: 'post',
        data:params
    })
}
//所有工种获取
export function getAllStudyWorkerTypeList(params){
    return request({
        url: '/StudyWorkerType/selectList',
        method: 'post',
        data:params
    })
}
export function formatDate(date) {
    let Y = date.getFullYear() + '-';
    let M = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) + '-' : date.getMonth() + 1 + '-';
    let D = date.getDate() < 10 ? '0' + date.getDate() + ' ' : date.getDate() + ' ';
    let h = date.getHours() < 10 ? '0' + date.getHours() + ':' : date.getHours() + ':';
    let m = date.getMinutes()  < 10 ? '0' + date.getMinutes() + ':' : date.getMinutes() + ':';
    let s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
    return Y + M + D + h + m + s;
}