import request from '@/utils/request'
/////公告管理
    /*公告添加*/
 export function addNotice(params){
    return request({
        url: '/Notice/add',
        method: 'post',
        data:params
    })
}
    /*公告删除*/
 export function deleteNotice(params){
        return request({
            url: '/Notice/delete',
            method: 'post',
            data:params
        })
}
    /*列表获取*/
 export function getNoticeList(params){
    return request({
        url: '/Notice/select',
        method: 'post',
        data:params
    })
}
     /** 用户列表获取*/
 export function getAllUserList(params){
    return request({
        url: '/JasoUser/selectList',
        method: 'post',
        data:params
    })
}
export function formatDate(date) {
    let Y = date.getFullYear() + '-';
    let M = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) + '-' : date.getMonth() + 1 + '-';
    let D = date.getDate() < 10 ? '0' + date.getDate() + ' ' : date.getDate() + ' ';
    let h = date.getHours() < 10 ? '0' + date.getHours() + ':' : date.getHours() + ':';
    let m = date.getMinutes()  < 10 ? '0' + date.getMinutes() + ':' : date.getMinutes() + ':';
    let s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
    return Y + M + D + h + m + s;
}
