import request from '@/utils/request'

export function addSecurityCheck(params) {
  return request({
    url: '/SecurityCheck/add',
    method: 'post',
    data: params
  })
}

export function getSecurityCheckList(params) {
  return request({
    url: '/SecurityCheck/select',
    method: 'post',
    data: params
  })
}
export function getAllSecurityCheckList(params) {
  return request({
    url: '/SecurityCheck/selectList',
    method: 'post',
    data: params
  })
}

export function deleteSecurityCheckList(params) {
  return request({
    url: '/SecurityCheck/delete',
    method: 'post',
    data: params
  })
}

export function updateSecurityCheck(params) {
  return request({
    url: '/SecurityCheck/update',
    method: 'post',
    data: params
  })
}

export function setSecurityCheckProcess(params) {
  return request({
    url: '/SecurityCheck/setProcess',
    method: 'post',
    data: params
  })
}

export function getListByProjectId(params) {
  return request({
    url: '/JasoUser/getListByProjectId',
    method: 'post',
    data: params
  })
}

export function sendRectifyUserList(params) {
  return request({
    url: '/SecurityCheck/send',
    method: 'post',
    data: params
  })
}

export function checkSecurity(params) {
  return request({
    url: '/SecurityCheck/check',
    method: 'post',
    data: params
  })
}

export function getNatureList(params) {
  return request({
    url: '/Nature/selectList',
    method: 'post',
    data: params
  })
}

export function addSecurityFine(params) {
  return request({
    url: '/SecurityFine/add',
    method: 'post',
    data: params
  })
}

export function getAboutUserList(params) {
  return request({
    url: '/JasoUser/getUserListByProjectId',
    method: 'post',
    data: params
  })
}

export function getAboutDepartmentList(params) {
  return request({
    url: '/Department/selectDepartmentByProjectId',
    method: 'post',
    data: params
  })
}

export function getSecurityFine(params) {
  return request({
    url: '/SecurityFine/select',
    method: 'post',
    data: params
  })
}

export function deleteSecurityFine(params) {
  return request({
    url: '/SecurityFine/delete',
    method: 'post',
    data: params
  })
}
