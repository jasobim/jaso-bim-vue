import request from '@/utils/request'

//图片轮播管理
    /*列表获取*/
    export function getImageRotationList(params){
        return request({
            url: '/ImageRotation/select',
            method: 'post',
            data:params
        })
    }
    export function addImageRotation(params){
        return request({
            url: '/ImageRotation/add',
            method: 'post',
            data:params
        })
    }
    export function deleteImageRotation(params){
        return request({
            url: '/ImageRotation/delete',
            method: 'post',
            data:params
        })
    }
    export function formatDate(date) {
        let Y = date.getFullYear() + '-';
        let M = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) + '-' : date.getMonth() + 1 + '-';
        let D = date.getDate() < 10 ? '0' + date.getDate() + ' ' : date.getDate() + ' ';
        let h = date.getHours() < 10 ? '0' + date.getHours() + ':' : date.getHours() + ':';
        let m = date.getMinutes()  < 10 ? '0' + date.getMinutes() + ':' : date.getMinutes() + ':';
        let s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
        return Y + M + D + h + m + s;
    }