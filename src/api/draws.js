import request from '@/utils/request'

export function addProjectPaper(params) {
  return request({
    url: '/ProjectPaper/add',
    method: 'post',
    data: params
  })
}

export function deleteProjectPaperList(params) {
  return request({
    url: '/ProjectPaper/delete',
    method: 'post',
    data: params
  })
}

export function getProjectPaperList(params) {
  return request({
    url: '/ProjectPaper/select',
    method: 'post',
    data: params
  })
}
