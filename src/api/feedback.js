import request from '@/utils/request'
//意见反馈管理
/*新增*/
export function addFeedback(params){
    return request({
        url: '/Feedback/add',
        method: 'post',
        data:params
    })
}
/*删除*/
export function deleteFeedbackList(params){
    return request({
        url: '/Feedback/delete',
        method: 'post',
        data:params
    })
}
/** 列表获取*/
export function getFeedbackList(params){
    return request({
        url: '/Feedback/select',
        method: 'post',
        data:params
    })
}
export function formatDate(date) {
    let Y = date.getFullYear() + '-';
    let M = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) + '-' : date.getMonth() + 1 + '-';
    let D = date.getDate() < 10 ? '0' + date.getDate() + ' ' : date.getDate() + ' ';
    let h = date.getHours() < 10 ? '0' + date.getHours() + ':' : date.getHours() + ':';
    let m = date.getMinutes()  < 10 ? '0' + date.getMinutes() + ':' : date.getMinutes() + ':';
    let s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
    return Y + M + D + h + m + s;
}