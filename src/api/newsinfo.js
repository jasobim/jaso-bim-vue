import request from '@/utils/request'
  //新闻资讯管理
    /*新增*/
    export function addNewsInfo(params){
        return request({
            url: '/NewsInfo/add',
            method: 'post',
            data:params
        })
    }
    /*删除*/
    export function deleteNewsInfoList(params){
        return request({
            url: '/NewsInfo/delete',
            method: 'post',
            data:params
        })
    }
    /** 列表获取*/
    export function getNewsInfoList(params){
        return request({
            url: '/NewsInfo/select',
            method: 'post',
            data:params
        })
    }
    export function formatDate(date) {
        let Y = date.getFullYear() + '-';
        let M = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) + '-' : date.getMonth() + 1 + '-';
        let D = date.getDate() < 10 ? '0' + date.getDate() + ' ' : date.getDate() + ' ';
        let h = date.getHours() < 10 ? '0' + date.getHours() + ':' : date.getHours() + ':';
        let m = date.getMinutes()  < 10 ? '0' + date.getMinutes() + ':' : date.getMinutes() + ':';
        let s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
        return Y + M + D + h + m + s;
    }
