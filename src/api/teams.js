import request from '@/utils/request'

export function addProjectTeams(params) {
  return request({
    url: '/ProjectTeams/add',
    method: 'post',
    data: params
  })
}

export function deleteProjectTeams(params) {
  return request({
    url: '/ProjectTeams/delete',
    method: 'post',
    data: params
  })
}

export function getProjectTeamsList(params) {
  return request({
    url: '/ProjectTeams/select',
    method: 'post',
    data: params
  })
}
