import request from '@/utils/request'

export function addWorkTask(params) {
  return request({
    url: '/WorkTask/add',
    method: 'post',
    data: params
  })
}

export function acceptWorkTask(params) {
  return request({
    url: '/WorkTask/accept',
    method: 'post',
    data: params
  })
}

export function setProcess(params) {
  return request({
    url: '/WorkTask/setProcess',
    method: 'post',
    data: params
  })
}

export function checkWorkTask(params) {
  return request({
    url: '/WorkTask/check',
    method: 'post',
    data: params
  })
}

export function deleteWorkTask(params) {
  return request({
    url: '/WorkTask/delete',
    method: 'post',
    data: params
  })
}

export function getWorkTaskList(params) {
  return request({
    url: '/WorkTask/select',
    method: 'post',
    data: params
  })
}
