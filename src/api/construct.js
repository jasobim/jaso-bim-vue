import request from '@/utils/request'

export function getConstructLogRole(params) {
  return request({
    url: '/ConstructLog/getRoleType',
    method: 'post',
    data: params
  })
}

export function getAllConstructContentType(params) {
  return request({
    url: '/ConstructContentType/selectList',
    method: 'post',
    data: params
  })
}

export function getAllExistingProblem(params) {
  return request({
    url: '/ExistingProblem/selectList',
    method: 'post',
    data: params
  })
}

export function getAllSubjectMaterial(params) {
  return request({
    url: '/SubjectMaterial/selectList',
    method: 'post',
    data: params
  })
}

export function getAllMachinery(params) {
  return request({
    url: '/Machinery/selectList',
    method: 'post',
    data: params
  })
}

export function getProjectBuildingTree(params) {
  return request({
    url: '/ProjectBuilding/selectTree',
    method: 'post',
    data: params
  })
}

export function getConstructContentByRole(params) {
  return request({
    url: '/RoleConstruct/selectList',
    method: 'post',
    data: params
  })
}

export function addConstructLog(params) {
  return request({
    url: '/ConstructLog/add',
    method: 'post',
    data: params
  })
}

export function getConstructLogList(params) {
  return request({
    url: '/ConstructLog/select',
    method: 'post',
    data: params
  })
}

export function deleteConstructLog(params) {
  return request({
    url: '/ConstructLog/delete',
    method: 'post',
    data: params
  })
}

export function importConstructContentList(params) {
  return request({
    url: '/ConstructContent/web/importConstructContent',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
  })
}

export function getAllConstructContent(params) {
  return request({
    url: '/ConstructContent/selectList',
    method: 'post',
    data: params
  })
}

export function addConstructContent(params) {
  return request({
    url: '/ConstructContent/add',
    method: 'post',
    data: params
  })
}

export function deleteConstructContent(params) {
  return request({
    url: '/ConstructContent/delete',
    method: 'post',
    data: params
  })
}

export function getConstructContentTree(params) {
  return request({
    url: '/ConstructContent/selectTree',
    method: 'post',
    data: params
  })
}

export function getConstructContentList(params) {
  return request({
    url: '/ConstructContent/select',
    method: 'post',
    data: params
  })
}

export function deleteDepartment(params) {
  return request({
    url: '/Department/delete',
    method: 'post',
    data: params
  })
}

export function addConstructContentType(params) {
  return request({
    url: '/ConstructContentType/add',
    method: 'post',
    data: params
  })
}

export function getConstructContentTypeList(params) {
  return request({
    url: '/ConstructContentType/select',
    method: 'post',
    data: params
  })
}

export function deleteConstructContentTypeList(params) {
  return request({
    url: '/ConstructContentType/delete',
    method: 'post',
    data: params
  })
}

export function getUserPcDepartmentTreeList(params) {
  return request({
    url: '/JasoUser/selectDepartmentTree',
    method: 'post',
    data: params
  })
}

export function addOrUpdateRoleConstruct(params) {
  return request({
    url: '/RoleConstruct/add',
    method: 'post',
    data: params
  })
}

export function deleteRoleConstructList(params) {
  return request({
    url: '/RoleConstruct/delete',
    method: 'post',
    data: params
  })
}

export function getRoleConstructList(params) {
  return request({
    url: '/RoleConstruct/select',
    method: 'post',
    data: params
  })
}

export function getExistingProblemList(params) {
  return request({
    url: '/ExistingProblem/select',
    method: 'post',
    data: params
  })
}

export function addExistingProblem(params) {
  return request({
    url: '/ExistingProblem/add',
    method: 'post',
    data: params
  })
}

export function deleteExistingProblemList(params) {
  return request({
    url: '/ExistingProblem/delete',
    method: 'post',
    data: params
  })
}

export function getMachineryList(params) {
  return request({
    url: '/Machinery/select',
    method: 'post',
    data: params
  })
}

export function addMachinery(params) {
  return request({
    url: '/Machinery/add',
    method: 'post',
    data: params
  })
}

export function deleteMachineryList(params) {
  return request({
    url: '/Machinery/delete',
    method: 'post',
    data: params
  })
}

export function addSubjectMaterial(params) {
  return request({
    url: '/SubjectMaterial/add',
    method: 'post',
    data: params
  })
}

export function deleteSubjectMaterialList(params) {
  return request({
    url: '/SubjectMaterial/delete',
    method: 'post',
    data: params
  })
}

export function getSubjectMaterialList(params) {
  return request({
    url: '/SubjectMaterial/select',
    method: 'post',
    data: params
  })
}
export function updateProjectCityCodeInfo(params){
  return request({
    url: '/Project/updateCityCode',
    method: 'post',
    data: params
  })
}
/*施工进度*/
    //班组获取
export function getAllProjectTeam(params){
  return request({
    url: '/ProjectTeams/selectList',
    method: 'post',
    data: params
  })
}
export function formatDate(date) {
  let Y = date.getFullYear() + '-';
  let M = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) + '-' : date.getMonth() + 1 + '-';
  let D = date.getDate() < 10 ? '0' + date.getDate() + ' ' : date.getDate() + ' ';
  return Y + M + D;
}
