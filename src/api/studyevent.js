import request from '@/utils/request'
export function formatDate(date) {
    let Y = date.getFullYear() + '-';
    let M = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) + '-' : date.getMonth() + 1 + '-';
    let D = date.getDate() < 10 ? '0' + date.getDate() + ' ' : date.getDate() + ' ';
    let h = date.getHours() < 10 ? '0' + date.getHours() + ':' : date.getHours() + ':';
    let m = date.getMinutes()  < 10 ? '0' + date.getMinutes() + ':' : date.getMinutes() + ':';
    let s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
    return Y + M + D + h + m + s;
  }
  //学习时间接口
  //新增
  export function addStudyEvent(params){
    return request({
        url: '/StudyEvent/add',
        method: 'post',
        data:params
    })
}
//删除
export function deleteStudyEvent(params){
    return request({
        url:'/StudyEvent/delete',
        data:params,
        method: 'post'
    })
}
//获取列表
export function getStudyEventList(params){
    return request({
        url:'/StudyEvent/select',
        data:params,
        method: 'post'
    })
}