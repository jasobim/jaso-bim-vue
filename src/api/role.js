import request from '@/utils/request'
export function formatDate(date) {
    let Y = date.getFullYear() + '-';
    let M = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) + '-' : date.getMonth() + 1 + '-';
    let D = date.getDate() < 10 ? '0' + date.getDate() + ' ' : date.getDate() + ' ';
    let h = date.getHours() < 10 ? '0' + date.getHours() + ':' : date.getHours() + ':';
    let m = date.getMinutes()  < 10 ? '0' + date.getMinutes() + ':' : date.getMinutes() + ':';
    let s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
    return Y + M + D + h + m + s;
}
///角色管理
/*角色类型新增*/
 export function addRole(params){
    return request({
        url: '/Role/add',
        method: 'post',
        data:params
    })
}
/*分页查询*/
export function getRoleList(params){
    return request({
        url: '/Role/select',
        method: 'post',
        data:params
    })
}
/*all查询*/
export function getAllRoleList(params){
    return request({
        url: '/Role/selectList',
        method: 'post',
        data:params
    })
}
/*根据用户id查询所选角色*/
export function getRoleListByUserId(params){
    return request({
        url: '/JasoUser/getRoleListByUserId',
        method: 'post',
        data:params
    })
}
//权限设置
export function roleSetting(params){
    return request({
        url: '/Role/roleSet',
        method: 'post',
        data:params
    })
}
//获取所有菜单列表
 export function selectAllMenuList(params){
    return request({
        url: '/Menu/selectAllList',
        method: 'post',
        data:params
    })
}
//获取当前角色所对应的菜单
 export function selectRoleMenuList(params){
    return request({
        url: '/Role/selectRoleMenuList',
        method: 'post',
        data:params
    })
}