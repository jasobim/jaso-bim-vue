import request from '@/utils/request'

export function addProjectTenders(params) {
  return request({
    url: '/ProjectTenders/add',
    method: 'post',
    data: params
  })
}

export function deleteProjectTenders(params) {
  return request({
    url: '/ProjectTenders/delete',
    method: 'post',
    data: params
  })
}

export function getProjectTendersList(params) {
  return request({
    url: '/ProjectTenders/select',
    method: 'post',
    data: params
  })
}
