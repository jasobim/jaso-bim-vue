import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/JasoUser/loginPc',
    method: 'post',
    data: {
      userName: data.username,
      password: data.password
    }
  })
}

export function getInfo(token) {
  return request({
    url: '/JasoUser/getJasoUserMessage',
    method: 'get'
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'post'
  })
}
//用户管理接口

    //PC端用户管理控制器

    /*列表获取*/
    export function getUserPcList(params) {
      return request({
        url: '/JasoUser/select',
        data: params,
        method: 'post'
      })
    }
    /*新增*/
    export function addUserPc(params){
      return request({
        url: '/JasoUser/add',
        data: params,
        method: 'post'
      })
    }
    /*删除*/
    export function deleteUserPc(params){
      return request({
        url: '/JasoUser/delete',
        data: params,
        method: 'post'
      })
    }
    /*用户详情加载(用户角色)*/
    export function getUserPcDetail(params){
      return request({
        url: '/JasoUser/selectDetail',
        data: params,
        method: 'post'
      })
    }
    /*用户的组织架构id查询*/
    export function getUserDepartmentList(params){
      return request({
        url: '/JasoUser/selectDepartmentDetail',
        data: params,
        method: 'post'
      })
    }
    export function getUserPcDepartmentTreeList(params){
      return request({
        url: '/JasoUser/selectDepartmentTree',
        data: params,
        method: 'post'
      })
    }
    /** 获取用户选中的项目列表*/
    export function getProjectListByUser(params){
      return request({
        url: '/UserProject/selectProjectList',
        data: params,
        method: 'post'
      })
    }
    //公用tree方法
    export function selectDepartmentTree(params){
      return request({
        url: '/Department/selectTree',
        data: params,
        method: 'post'
      })
    }
    //获取所有的角色列表
    export function getAllRoleList(params){
      return request({
        url: '/Role/selectList',
        data: params,
        method: 'post'
      })
    }
    //获取所有项目列表
    export function selectProjectLists(params){
      return request({
        url: '/Project/selectList',
        data: params,
        method: 'post'
      })
    }
    //获取所有工种列表
    export function getWorkTypeListByRoleId(params){
      return request({
        url: '/WorkType/selectList',
        data: params,
        method: 'post'
      })
    }
    //获取用户所选的工种列表
    export function getWorkTypeListByUserId(params){
      return request({
        url:'/WorkType/selectByUserId',
        data: params,
        method: 'post'
      })
    }

