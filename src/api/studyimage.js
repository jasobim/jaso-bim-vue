import request from '@/utils/request'
export function formatDate(date) {
    let Y = date.getFullYear() + '-';
    let M = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) + '-' : date.getMonth() + 1 + '-';
    let D = date.getDate() < 10 ? '0' + date.getDate() + ' ' : date.getDate() + ' ';
    let h = date.getHours() < 10 ? '0' + date.getHours() + ':' : date.getHours() + ':';
    let m = date.getMinutes()  < 10 ? '0' + date.getMinutes() + ':' : date.getMinutes() + ':';
    let s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
    return Y + M + D + h + m + s;
  }
  //学习轮播图片接口
  //新增
export function addStudyImage(params){
    return request({
        url: '/StudyImage/add',
        method: 'post',
        data:params
    })
}
export function deleteStudyImage(params){
    return request({
        url: '/StudyImage/delete',
        method: 'post',
        data:params
    })
}
export function getStudyImageList(params){
    return request({
        url: '/StudyImage/select',
        method: 'post',
        data:params
    })
}