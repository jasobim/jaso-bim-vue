import request from '@/utils/request'

export function getProjectList() {
  return request({
    url: '/Project/selectList',
    method: 'post',
    data: {
      buildingUnit: ''
    }
  })
}

export function selectProjectLists(params) {
  return request({
    url: '/Project/selectList',
    method: 'post',
    data: params
  })
}


export function getProjectTendersListByProjectId(params) {
  return request({
    url: '/ProjectTenders/selectList',
    method: 'post',
    data: params
  })
}

export function getAllRoleList(params) {
  return request({
    url: '/Role/selectList',
    method: 'post',
    data: params
  })
}
//项目管理接口
//新增
export function addProject(params){
  return request({
    url: '/Project/add',
    method: 'post',
    data: params
  })
}
//删除
export function deleteProject(params){
  return request({
    url: '/Project/delete',
    method: 'post',
    data: params
  })
}
//获取列表
export function getProjectListByPage(params){
  return request({
    url: '/Project/select',
    method: 'post',
    data: params
  })
}
 //获取所有用户
export function selectAllUserList(params){
  return request({
    url: '/JasoUser/selectList',
    method: 'post',
    data: params
  })
}
  /*所有城市编码获取*/
export function getAllCityCode(params){
  return request({
    url: '/Project/getCityCode',
    method: 'post',
    data: params
  })
}
export function formatDate1(date) {
  let Y = date.getFullYear() + '-'
  let M = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) + '-' : date.getMonth() + 1 + '-'
  let D = date.getDate() < 10 ? '0' + date.getDate() + ' ' : date.getDate() + ' '
  let h = date.getHours() < 10 ? '0' + date.getHours() + ':' : date.getHours() + ':'
  let m = date.getMinutes() < 10 ? '0' + date.getMinutes() + ':' : date.getMinutes() + ':'
  let s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds()
  return Y + M + D + h + m + s
}
