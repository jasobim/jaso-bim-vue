import request from '@/utils/request'

export function addProjectCheckType(params) {
  return request({
    url: '/ProjectCheckType/add',
    method: 'post',
    data: params
  })
}

export function deleteProjectCheckTypeList(params) {
  return request({
    url: '/ProjectCheckType/delete',
    method: 'post',
    data: params
  })
}

export function getProjectCheckTypeList(params) {
  return request({
    url: '/ProjectCheckType/select',
    method: 'post',
    data: params
  })
}
