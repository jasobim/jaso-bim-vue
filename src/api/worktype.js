import request from '@/utils/request'
/////工种管理
    /*工种添加*/
    export function addWorkType(params){
        return request({
            url: '/WorkType/add',
            method: 'post',
            data:params
        })
    }
        /*工种删除*/
     export function deleteWorkType(params){
            return request({
                url: '/WorkType/delete',
                method: 'post',
                data:params
            })
    }
        /*列表获取*/
     export function getWorkTypeList(params){
        return request({
            url: '/WorkType/select',
            method: 'post',
            data:params
        })
    }
      /*角色查询*/
     export function getAllRoleList(params){
        return request({
            url: '/Role/selectList',
            method: 'post',
            data:params
        })
    }
    //所有工种获取
    export function getAllWorkType(params){
        return request({
            url:'/WorkType/selectList',
            method: 'post',
            data:params
        })
    }
    export function formatDate(date) {
        let Y = date.getFullYear() + '-';
        let M = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) + '-' : date.getMonth() + 1 + '-';
        let D = date.getDate() < 10 ? '0' + date.getDate() + ' ' : date.getDate() + ' ';
        let h = date.getHours() < 10 ? '0' + date.getHours() + ':' : date.getHours() + ':';
        let m = date.getMinutes()  < 10 ? '0' + date.getMinutes() + ':' : date.getMinutes() + ':';
        let s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
        return Y + M + D + h + m + s;
    }
    