import request from '@/utils/request'

export function getProjectBuildingList(params) {
  return request({
    url: '/ProjectBuilding/select',
    method: 'post',
    data: params
  })
}

export function getAllProjectBuildingLists(params) {
  return request({
    url: '/ProjectBuilding/selectByPid',
    method: 'post',
    data: params
  })
}

export function addProjectBuilding(params) {
  return request({
    url: '/ProjectBuilding/edit',
    method: 'post',
    data: params
  })
}
export function updateProjectBuilding(params) {
  return request({
    url: '/ProjectBuilding/update',
    method: 'post',
    data: params
  })
}
//获取所有户型
export function getAllApartmentList(params){
  return request({
    url: '/Apartment/selectList',
    method: 'post',
    data: params
  })
}

export function deleteProjectBuilding(params) {
  return request({
    url: '/ProjectBuilding/delete',
    method: 'post',
    data: params
  })
}

export function deleteProjectBuildingByPid(params) {
  return request({
    url: '/ProjectBuilding/deleteByPid',
    method: 'post',
    data: params
  })
}

export function getProjectBuildingListTree(params) {
  return request({
    url: '/ProjectBuilding/selectTree',
    method: 'post',
    data: params
  })
}
export function getBuildingInfoList(params){
  return request({
    url: '/ProjectBuilding/selectList',
    method: 'post',
    data: params
  })
}
export function getRoomInfoList(params){
  return request({
    url: '/ProjectBuilding/selectListByPid',
    method: 'post',
    data: params
  })
}
