import request from '@/utils/request'
/** 工种类型管理*/
//工种类型添加、编辑
export function addStudyWorkerType(params){
    return request({
        url: '/StudyWorkerType/add',
        method: 'post',
        data:params
    })
}
 //工种类型列表获取
export function getStudyWorkerTypeList(params){
    return request({
        url: '/StudyWorkerType/select',
        method: 'post',
        data:params
    })
}
//工种类型删除
export function deleteStudyWorkerType(params){
    return request({
        url: '/StudyWorkerType/delete',
        method: 'post',
        data:params
    })
}
//所有工种类型获取
export function getAllStudyWorkerTypeList(params){
    return request({
        url: '/StudyWorkerType/selectList',
        method: 'post',
        data:params
    })
}
export function formatDate(date) {
    let Y = date.getFullYear() + '-';
    let M = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) + '-' : date.getMonth() + 1 + '-';
    let D = date.getDate() < 10 ? '0' + date.getDate() + ' ' : date.getDate() + ' ';
    let h = date.getHours() < 10 ? '0' + date.getHours() + ':' : date.getHours() + ':';
    let m = date.getMinutes()  < 10 ? '0' + date.getMinutes() + ':' : date.getMinutes() + ':';
    let s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
    return Y + M + D + h + m + s;
}