import request from '@/utils/request'
//学习资料管理
//学习资料列表获取
export function getStudyFileList(params){
    return request({
        url: '/StudyFile/select',
        method: 'post',
        data:params
    })
}
//学习资料新增
export function addStudyFile(params){
    return request({
        url: '/StudyFile/add',
        method: 'post',
        data:params
    })
}
//学习资料删除
export function deleteStudyFile(params){
    return request({
        url: '/StudyFile/delete',
        method: 'post',
        data:params
    })
}
//所有工种获取
export function getAllStudyWorkerTypeList(params){
    return request({
        url: '/StudyWorkerType/selectList',
        method: 'post',
        data:params
    })
}
export function formatDate(date) {
    let Y = date.getFullYear() + '-';
    let M = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) + '-' : date.getMonth() + 1 + '-';
    let D = date.getDate() < 10 ? '0' + date.getDate() + ' ' : date.getDate() + ' ';
    let h = date.getHours() < 10 ? '0' + date.getHours() + ':' : date.getHours() + ':';
    let m = date.getMinutes()  < 10 ? '0' + date.getMinutes() + ':' : date.getMinutes() + ':';
    let s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
    return Y + M + D + h + m + s;
}  