import axios from 'axios'
import request from '@/utils/request'

export function getPositionName(params) {
  params.output = 'JSON'
  params.key = '8fd4655c8ad5b554912b1aa8c6ab1e89'
  params.radius = 0
  params.extensions = 'base'
  return axios.get('http://restapi.amap.com/v3/geocode/regeo', { params: params })
}
export function insert(params) {
  return request({
    url: '/WorkPosition/insert',
    method: 'post',
    data: params
  })
}
export function select(params) {
  return request({
    url: '/WorkPosition/select',
    method: 'post',
    data: params
  })
}
export function del(params) {
  return request({
    url: '/WorkPosition/delete',
    method: 'post',
    data: params
  })
}

export function options(params) {
  return request({
    url: '/WorkDepartmentRule/options',
    method: 'post',
    data: params
  })
}

export function insertWorkDepartmentRule(params) {
  return request({
    url: '/WorkDepartmentRule/insert',
    method: 'post',
    data: params
  })
}
export function selectWorkDepartmentRule(params) {
  return request({
    url: '/WorkDepartmentRule/select',
    method: 'post',
    data: params
  })
}