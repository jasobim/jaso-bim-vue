import request from '@/utils/request'

export function addQualityCheck(params) {
  return request({
    url: '/QualityCheck/add',
    method: 'post',
    data: params
  })
}

export function deleteQualityCheckList(params) {
  return request({
    url: '/QualityCheck/delete',
    method: 'post',
    data: params
  })
}

export function getQualityCheckList(params) {
  return request({
    url: '/QualityCheck/select',
    method: 'post',
    data: params
  })
}
//质量奖惩事由
export function getAllQuality(params){
  return request({
    url: '/QualityCheck/selectList',
    method: 'post',
    data: params
  })
}

export function updateQualityCheck(params) {
  return request({
    url: '/QualityCheck/update',
    method: 'post',
    data: params
  })
}

export function setQualityCheckProcess(params) {
  return request({
    url: '/QualityCheck/setProcess',
    method: 'post',
    data: params
  })
}

export function sendRectifyUserList(params) {
  return request({
    url: '/QualityCheck/send',
    method: 'post',
    data: params
  })
}

export function checkQuality(params) {
  return request({
    url: '/QualityCheck/check',
    method: 'post',
    data: params
  })
}

export function getNatureList(params) {
  return request({
    url: '/Nature/selectList',
    method: 'post',
    data: params
  })
}

export function addQualityFine(params) {
  return request({
    url: '/QualityFine/add',
    method: 'post',
    data: params
  })
}

export function getAboutUserList(params) {
  return request({
    url: '/JasoUser/getUserListByProjectId',
    method: 'post',
    data: params
  })
}

export function getAboutDepartmentList(params) {
  return request({
    url: '/Department/selectDepartmentByProjectId',
    method: 'post',
    data: params
  })
}

export function getQualityFine(params) {
  return request({
    url: '/QualityFine/select',
    method: 'post',
    data: params
  })
}

export function deleteQualityFine(params) {
  return request({
    url: '/QualityFine/delete',
    method: 'post',
    data: params
  })
}
