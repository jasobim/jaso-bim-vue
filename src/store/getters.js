const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  projects: state => state.user.allProjects,
  userInfo: state => state.user.userInfo,
  currentProject: state => state.user.currentProject,
  isOperations: state => state.app.isOperations,
  menuList: state => state.user.includeMenuList,
  isAdmin: state => state.user.isAdmin
}
export default getters
