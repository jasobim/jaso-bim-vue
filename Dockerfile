FROM nginx:latest

RUN mkdir /etc/nginx/conf

WORKDIR /etc/nginx

ADD nginx/nginx.conf /etc/nginx/nginx.conf
ADD nginx/api.conf /etc/nginx/vhost/api.conf

ADD dist/ /usr/share/nginx/html/
